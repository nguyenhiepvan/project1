# **PROJECT1**
## **Đề tài: Viết chương trình quản lý nhân sự, tiền lương**
## Giảng viên hướng dẫn: PGS.TS Đặng Văn Chuyết
## Sinh viên thực hiện: Nguyễn Văn Hiệp
## Lớp: VUWIT15
## MSSV: 20158142
## Email: nguyenhiepvan.bka@gmail.com
**I. Bản quyền**

- Bản quyền thuộc về SV. Nguyễn Văn Hiệp, Sinh viên Viện CNTT-TT, Trường ĐHBK Hà Nội.
- Đây là mã nguồn được tạo ra để hoàn thành bộ môn PROJECT1
- Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập , cần có sự cho phép của SV. N.V.Hiệp.
- Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền
- Khi sử dụng cần nói rõ nguồn sử dụng

**II. Hướng dẫn:**

- Khuyến cáo nên chạy trên nền tảng windows.
- Để chạy chương trình. Mở file Project1.exe
