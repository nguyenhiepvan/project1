/*
- Bản quyền thuộc về SV. Nguyễn Văn Hiệp, Sinh viên Viện CNTT-TT, Trường ĐHBK Hà Nội.
- Đây là mã nguồn được tạo ra để hoàn thành bộ môn PROJECT1
- Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập , cần có sự cho phép của SV. N.V.Hiệp.
- Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền
- Khi sử dụng cần nói rõ nguồn sử dụng
- Email: nguyenhiepvan.bka@gmail.com
*/

#include <iostream>
#include "staff.h"
#include "phongBan.h"
#include <vector>
#include <string>

using namespace std;

#ifndef PROJECT1_H
#define PROJECT1_H

class Project1
{
	private:
		vector<PhongBan> danh_sach_phong_ban;
	public:
		Project1();
		void 	set_danh_sach(vector<PhongBan>);
		vector<PhongBan> get_danh_sach();
		void 	Them_phong_ban_moi();
		Staff 	Them_nhan_su_moi(int,int);
		void 	Xem_nhan_su_hien_co(vector<Staff>);
		void 	Tim_kiem_nhan_su(PhongBan,string);
		void 	Get_phong_ban_hien_co();
		void  	show_data_phong_ban();
		void 	Menu();
		void 	Thong_Tin_Chi_Tiet(PhongBan);
		void	save_Data();
		void 	Menu_Nhan_Vien(PhongBan);
		
};
#endif
