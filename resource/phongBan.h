/*
- Bản quyền thuộc về SV. Nguyễn Văn Hiệp, Sinh viên Viện CNTT-TT, Trường ĐHBK Hà Nội.
- Đây là mã nguồn được tạo ra để hoàn thành bộ môn PROJECT1
- Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập , cần có sự cho phép của SV. N.V.Hiệp.
- Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền
- Khi sử dụng cần nói rõ nguồn sử dụng
- Email: nguyenhiepvan.bka@gmail.com
*/
#include <iostream>
#include <string>
#include "staff.h"
#include <vector>

#ifndef PHONGBAN_H
#define PHONGBAN_H

class PhongBan
{
	private:
		string ten_phong_ban;
		string dia_chi;
		int ID;
		vector<Staff> danh_sach_nhan_su;
		long 	sumSalary;
		int 	so_luong_nhan_su;
	public:
		PhongBan();
		PhongBan(string,string,int);
		void 	setID(int);
		void 	setTen(string);
		void 	setDiaChi(string);
		void 	setNhanSu(vector<Staff>);
		void 	set_sum_salary(long);
		void 	set_so_luong_nhan_su(int);
		string 	getTen();
		string 	getDiaChi();
		int 	getID();
		long 	get_sum_salary();
		int 	get_so_luong_nhan_su();
		vector<Staff> getNhanSu();
		void 	showData();
		void 	add_staff(Staff);
		vector<Staff> search_Staff(string);
		bool 	remove_Staff(int);
		void 	save_Staff_Data();
		void 	get_Data_Staff_From_File(ifstream&);
		
		
		/*
		void sort_report_by_Name();
		void sort_report_by_salary_increase();
		void sort_report_by_salary_decrease();*/
		
};

#endif
