/*
- Bản quyền thuộc về SV. Nguyễn Văn Hiệp, Sinh viên Viện CNTT-TT, Trường ĐHBK Hà Nội.
- Đây là mã nguồn được tạo ra để hoàn thành bộ môn PROJECT1
- Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập , cần có sự cho phép của SV. N.V.Hiệp.
- Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền
- Khi sử dụng cần nói rõ nguồn sử dụng
- Email: nguyenhiepvan.bka@gmail.com
*/

#include "staff.h"
#include <fstream>
#include <string>
#include <sstream> 
#include <cstdlib>
#include <ctime>

#define baseSalary 1390000
	float CGCC[3]={8.80,9.40,10.00};
	float  A31[6]={6.20,6.56,6.92,7.28,7.64,8.00};
	float  A32[6]={5.75,6.11,6.47,6.83,7.19,7.55};
	float  A21[8]={4.40,4.74,5.08,5.42,5.76,6.10,6.44,6.78};
	float  A22[8]={4.00,4.34,4.68,5.02,5.36,5.70,6.04,6.38};
	float   A1[9]={2.34,2.67,3.00,3.33,3.66,3.99,4.32,4.65,4.98};
	float  A0[10]={2.10,2.41,2.72,3.03,3.34,3.65,3.96,4.27,4.58,4.89};
	float   B[12]={1.86,2.06,2.26,2.46,2.66,2.86,3.06,3.26,3.46,3.66,3.86,4.06};
	float  C1[12]={1.65,1.83,2.01,2.19,2.37,2.55,2.73,2.91,3.09,3.27,3.45,3.63};
	float  C2[12]={1.50,1.68,1.86,2.04,2.22,2.40,2.58,2.76,2.94,3.12,3.30,3.48};
	float  C3[12]={1.35,1.53,1.71,1.89,2.07,2.25,2.43,2.61,2.79,2.97,3.15,3.33};
Staff::Staff()
	{
		this->id 			 = 0;
		this->fullName		 = "";
		this->phoneNumber	 = "";
		this->address 		 = "";
		this->position 		 = "";
		this->type			 = "";
		this->levelSalary	 = 0;
	}
Staff::Staff(int id,string fullName,string phoneNumber,string address,string position,string type,int levelSalary)
	{
		
		this->id			 = id;
		this->fullName		 = fullName;
		this->phoneNumber	 = phoneNumber;
		this->address 		 = address;
		this->position 		 = position;
		this->type			 = type;
		this->levelSalary	 = levelSalary;
	}
void Staff::setID(int ID)
{
	this->id = ID;
}
void Staff::setPhoneNumber(string phoneNumber)
	{
		this->phoneNumber	 = phoneNumber;
	}
void Staff::setFullName(string fullName)
	{
		this->fullName 		 = fullName;
	}
void Staff::setAdress(string address)
	{
		this->address 		 = address;
	}
void Staff::setPosition(string position)
	{
		this->position 		 = position;
	}
void Staff::setType(string type)
	{
		this->type 		 	 = type;
	}
void Staff::setLevelSalary(int levelSalary)
	{
		this->levelSalary 	 = levelSalary;
	}
int Staff::getID()
{
		return this->id;
}
string Staff::getFullName()
	{
		return this->fullName;
	}
string Staff::getphoneNumber()
	{
		return this->phoneNumber;
	}
string Staff::getAddress()
	{
		return this->address;
	}
string Staff::getPosition()
	{
		return this->position;
	}
string Staff::getType()
	{
		return this->type;
	}
int Staff::getLevelSalary()
	{
		return this->levelSalary;
	}
float Staff::getSalary()
	{
		if(this->type=="CGCC") 	return 	CGCC[this->levelSalary-1];
		if(this->type=="A31") 	return 	A31[this->levelSalary-1]; 
		if(this->type=="A32") 	return 	A32[this->levelSalary-1]; 
		if(this->type=="A21") 	return 	A21[this->levelSalary-1]; 
		if(this->type=="A22") 	return 	A22[this->levelSalary-1]; 
		if(this->type=="A1") 	return 	A1[this->levelSalary-1]; 
		if(this->type=="A0") 	return 	A0[this->levelSalary-1]; 
		if(this->type=="B") 	return 	B[this->levelSalary-1]; 
		if(this->type=="C1") 	return 	C1[this->levelSalary-1]; 
		if(this->type=="C2") 	return 	C2[this->levelSalary-1]; 
		if(this->type=="C3") 	return 	C3[this->levelSalary-1];
		return 0;
			
		
	}
long Staff::salary()
	{
		return baseSalary*getSalary();
	}
void Staff::display()
	{
		cout<<"ID: "<<getID()<<endl;
		cout<<"Ho va ten: "<<this->fullName<<endl;
		cout<<"Dia chi: "<<this->address<<endl;
		cout<<"So dien thoai: "<<this->phoneNumber<<endl;
		cout<<"Vi tri: "<<this->position<<endl;
		cout<<"Loai nhan vien: "<<this->type<<endl;
		cout<<"Bac luong: "<<getLevelSalary()<<endl;

	}
	


		
//int main()
//	{
		
		//Staff A("Dao van C","987654321","Ha Nam","Java Developer","CGCC",1);
		//cout<<A.salary()<<endl;
		//A.setID(0);
	//	A.display();
	//	
	//	ofstream outfile;
	//	outfile.open("file.dat",ios::app);
	//	// ghi du lieu da nhap vao trong file.
	//   outfile << A.getFullName()<<"|"<<A.getphoneNumber()<<"|"<<A.getAddress()<<"|"<<A.getPosition()<<"|"<<A.getType()<<"|"<<A.getLevelSalary()<<"|"<<A.getSalary()<<"|"<<A.salary() << endl;
	//
	//   // dong file da mo.
	//   outfile.close();
	   
	  

