/*
- Bản quyền thuộc về SV. Nguyễn Văn Hiệp, Sinh viên Viện CNTT-TT, Trường ĐHBK Hà Nội.
- Đây là mã nguồn được tạo ra để hoàn thành bộ môn PROJECT1
- Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập , cần có sự cho phép của SV. N.V.Hiệp.
- Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền
- Khi sử dụng cần nói rõ nguồn sử dụng
- Email: nguyenhiepvan.bka@gmail.com
*/

#include "phongBan.h"
#include "staff.h"
#include <iostream>
#include <string> 
#include <vector>
#include <sstream> 
#include <cstdlib>
#include <ctime>

using namespace std;

PhongBan::PhongBan()
{
	this->ID =0;
	this->ten_phong_ban = "";
	this->dia_chi = "";
	this->sumSalary =0;
	this->so_luong_nhan_su=0;
}
PhongBan::PhongBan(string ten,string diachi,int id)
{

	this->ID =id;
	this->ten_phong_ban = ten;
	this->dia_chi = diachi;
	this->sumSalary =0;
	this->so_luong_nhan_su=0;
}
void PhongBan::setID(int id)
{
	this->ID =id;
}
void PhongBan::setTen(string ten)
{
	this->ten_phong_ban = ten;
}
void PhongBan::setDiaChi(string dia_chi)
{
	this->dia_chi = dia_chi;
}
void PhongBan::setNhanSu(vector<Staff> danhSach)
{
	this->danh_sach_nhan_su = danhSach;
}
void PhongBan::set_sum_salary(long salary)
{
	this->sumSalary = salary;
}
void 	PhongBan::set_so_luong_nhan_su(int num)
{
	this->so_luong_nhan_su=num;
}
string PhongBan::getTen()
{
	return this->ten_phong_ban;
}
string PhongBan::getDiaChi()
{
	return this->dia_chi;
}
int PhongBan::getID()
{
	return this->ID;
}

long PhongBan::get_sum_salary()
{
	return this->sumSalary;
}
int PhongBan::get_so_luong_nhan_su()
{
	return this->so_luong_nhan_su;
}
vector<Staff> PhongBan::getNhanSu()
{
	return this->danh_sach_nhan_su;
}
void PhongBan::showData()
{
	cout <<"Phong ban: "<<getTen()<<endl;
	cout <<"Dia chi: "<<getDiaChi()<<endl;
	cout <<"ID: "<<getID()<<endl;
	long salary;
	for(int i=0;i<this->danh_sach_nhan_su.size();i++)
	{
		danh_sach_nhan_su.at(i).display();
		salary+=danh_sach_nhan_su.at(i).salary();
	}
	cout<<"Tong: "<<salary<<endl;
}

void PhongBan::add_staff(Staff temp)
{

	this->danh_sach_nhan_su.push_back(temp);
	this->so_luong_nhan_su++;
	this->sumSalary+=temp.salary();
}

vector<Staff> PhongBan::search_Staff(string key)
{
	vector<Staff> result;
	for(int i=0;i<this->danh_sach_nhan_su.size();i++)
	{
		if(this->danh_sach_nhan_su.at(i).getFullName().find(key)!=-1) result.push_back(this->danh_sach_nhan_su.at(i));
		else if(this->danh_sach_nhan_su.at(i).getphoneNumber().find(key)!=-1) result.push_back(this->danh_sach_nhan_su.at(i));
		else if(this->danh_sach_nhan_su.at(i).getAddress().find(key)!=-1) result.push_back(this->danh_sach_nhan_su.at(i));
		else if(this->danh_sach_nhan_su.at(i).getPosition().find(key)!=-1) result.push_back(this->danh_sach_nhan_su.at(i));
		else 
		{
			int a = this->danh_sach_nhan_su.at(i).getID();
			stringstream ss;
			ss << a;
			string str = ss.str();
			if(str.find(key)!=-1) result.push_back(this->danh_sach_nhan_su.at(i));	
		}	
	}
	return result;
}
bool PhongBan::remove_Staff(int id)
{
	for(int i=0;i<this->danh_sach_nhan_su.size();i++)
	if(id == this->danh_sach_nhan_su.at(i).getID())
	{
		this->so_luong_nhan_su--;
		this->sumSalary-=danh_sach_nhan_su.at(i).salary();
		this->danh_sach_nhan_su.erase(this->danh_sach_nhan_su.begin() + i);		
		return true;
	}
	return false;
}

void PhongBan::save_Staff_Data()
{
	ofstream outfile;
	outfile.open("staff.dat",ios::app);
	outfile <<"Phong ban:	"<<getTen()<<endl;
	outfile <<"Dia chi:	"<<getDiaChi()<<endl;
	outfile <<"ID:"<<getID()<<endl;
	outfile <<"So luong nhanh su:"<<get_so_luong_nhan_su()<<endl;
	for(int i=0;i<this->danh_sach_nhan_su.size();i++)
	outfile << this->danh_sach_nhan_su.at(i).getID()<<"	"<< this->danh_sach_nhan_su.at(i).getFullName()<<"	"<< this->danh_sach_nhan_su.at(i).getphoneNumber()<<"	"<< this->danh_sach_nhan_su.at(i).getAddress()<<"	"<< this->danh_sach_nhan_su.at(i).getPosition()<<"	"<< this->danh_sach_nhan_su.at(i).getType()<<"	"<< this->danh_sach_nhan_su.at(i).getLevelSalary()<< endl;
	outfile <<"----------------------------------------	"<<endl;
	outfile.close();
}

void PhongBan::get_Data_Staff_From_File(ifstream &filein)
{
	string result;
	int val;
	getline(filein,result);
	
	
	getline(filein,result,'\t');
	//cout<< result<<endl;
	getline(filein,result);
	//cout<< result<<endl;
	setTen(result);
	
	getline(filein,result,'\t');
	//cout<< result<<endl;
	getline(filein,result);
	//cout<< result<<endl;
	setDiaChi(result);
	
	getline(filein,result,':');
	//cout<< result<<endl;
	getline(filein,result);
	//cout<< result<<endl;
	stringstream geek(result);
	geek >> val;
	setID(val);
	
	getline(filein,result,':');
	getline(filein,result);
	//cout<<result<<endl;
	stringstream geek169(result);
	int num170;
	geek169 >> num170;
	//cout<<num170<<endl;
	//set_so_luong_nhan_su(num170);		
	
	for(int i=0;i<num170;i++)
	{	Staff temp;
		
		getline(filein,result,'\t');
		stringstream geek1(result);
		//cout<< result<<endl;
		geek1 >> val;
		temp.setID(val);
		
		getline(filein,result,'\t');
		//cout<< result<<endl;
		temp.setFullName(result);
	
		getline(filein, result,'\t');
		//cout<< result<<endl;
		temp.setPhoneNumber(result);
		
		getline(filein, result,'\t');
		//cout<< result<<endl;
		temp.setAdress(result);
		
		getline(filein,result,'\t');
		//cout<< result<<endl;
		temp.setPosition(result);
		
		getline(filein, result,'\t');
		//cout<< result<<endl;
		temp.setType(result);
		
		getline(filein, result);	
		//cout<< result <<endl;			
		stringstream geek2(result);
		geek2 >> val;
		temp.setLevelSalary(val);
		
		add_staff(temp);
	}	
}
/*
int main ()
{
	// g++ staff.cpp phongBan.cpp -o phongBan.exe
	
	ifstream filein;
	filein.open("staff.dat",ios::in);
	PhongBan test;
	test.get_Data_Staff_From_File(filein);

	cout<<"Enter keyword: ";
	cin.ignore();
	string key;
	cin>>key;
	vector<Staff> result = test.search_Staff(key);
	
	cout<<result.size();
	if(result.size()!=0)
	for(int i=0;i<result.size();i++)
	{
		cout<<"--------------------"<<endl;
		result.at(i).display();}
	return 0;
}
*/
