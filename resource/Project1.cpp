/*
- Bản quyền thuộc về SV. Nguyễn Văn Hiệp, Sinh viên Viện CNTT-TT, Trường ĐHBK Hà Nội.
- Đây là mã nguồn được tạo ra để hoàn thành bộ môn PROJECT1
- Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập , cần có sự cho phép của SV. N.V.Hiệp.
- Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền
- Khi sử dụng cần nói rõ nguồn sử dụng
- Email: nguyenhiepvan.bka@gmail.com
*/

#include <iostream>
#include "staff.h"
#include "Project1.h"
#include "phongBan.h"
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <cstring>
#include <conio.h>
#include <windows.h>

using namespace std;


Project1::Project1()
{
};

void Project1::set_danh_sach(vector<PhongBan> list)
{
	this->danh_sach_phong_ban = list;
}

vector<PhongBan> Project1::get_danh_sach()
{
	return this->danh_sach_phong_ban;
}

Staff Project1::Them_nhan_su_moi(int id,int lastid)
{
	string 	fullName;
	string 	phoneNumber;
	string 	address;
	string 	position;
	string 	type;
	int 	levelSalary;
	
	cin.ignore();
	
	do
	{
		
		cout<<"(*) LA BAT BUOC"<<endl;
		cout<<"Ho va Ten(*): "; 		getline(cin,fullName);
		//cin.ignore();
		cout<<"\nSo dien thoai(*): ";	getline(cin,phoneNumber);
		//cin.ignore();
		cout<<"\nDia chi(*): ";		getline(cin,address);
		//cin.ignore();
		cout<<"\nVi tri(*): ";		getline(cin,position);
		//cin.ignore();
	} while(fullName=="" || phoneNumber=="" || address=="" || position=="");
						
	do
	{
		cout<<"\nLoai(*)(CGCC,A31,A32,A21,A22,A1,A0,B,C1,C2,C3): ";getline(cin,type);
	} while(type!="CGCC"&&type!="A31"&&type!="A32"&&type!="A21"&&type!="A22"&&type!="A1"&&type!="A0"&&type!="B"&&type!="C1"&&type!="C2"&&type!="C3");
					
	
	//cin.ignore();
	BacLuong53:cout<<"\nBac luong(*): ";	scanf("%d",&levelSalary);
	if(type=="CGCC")
	{
		if(levelSalary<=0||levelSalary>3) 
		{
			cout<<" Bac luong khong hop le"<<endl;
			goto BacLuong53;
		}
	}
	else if(type=="A31"||type=="A32")
	{
		if(levelSalary<=0||levelSalary>6)
		{
			cout<<"Bac luong khong hop le"<<endl;
			goto BacLuong53;
		}
	}
	else if(type=="A21"||type=="A22")
	{
		if(levelSalary<=0||levelSalary>8)
		{ 
			cout<<"Bac luong khong hop le"<<endl;
			goto BacLuong53;
		}
	}
	else if(type=="A1")
	{
		if(levelSalary<=0||levelSalary>9)
		{
			cout<<"Bac luong khong hop le"<<endl;
			goto BacLuong53;
		}
	}
	else if(type=="A0")
	{
		if(levelSalary<=0||levelSalary>10)
		{			
			cout<<"Bac luong khong hop le"<<endl;
			goto BacLuong53;
		}
	}
	else if(type=="B"||type=="C1"||type=="C2"||type=="C3")
	{
		if(levelSalary<=0||levelSalary>12)
		{
			cout<<"Bac luong khong hop le"<<endl;
			goto BacLuong53;
		}
	}				
	
	Staff newbie(id*10000+lastid,fullName,phoneNumber,address,position,type,levelSalary);
	return newbie;
}

void  Project1::Xem_nhan_su_hien_co(vector<Staff> danhSach)
{
	if(danhSach.size()==0)
	{
		cout<<"CHUA CO NHAN SU NAO!"<<endl;
	}
	else
	cout<<"|------------------------------------------------------------------------------------------------------------------|"<<endl;
	cout<<"|						DANH SACH NHAN SU						   |"<<endl;
	cout<<"|------------------------------------------------------------------------------------------------------------------|"<<endl;
	printf("|%-9s | %-20s | %-11s | %-20s | %-20s | %-5s | %-9s  |","ID","ho va ten","sdt","dia chi","vi tri","loai","bac luong");
	cout<<"\n|------------------------------------------------------------------------------------------------------------------|";
	for(int i=0;i<danhSach.size();i++)
	{
		//danhSach.at(i).display();
		printf("\n|%-9d | %-20s | %-11s | %-20s | %-20s | %-5s |  %-8d  |",danhSach.at(i).getID(),danhSach.at(i).getFullName().c_str(),danhSach.at(i).getphoneNumber().c_str(),danhSach.at(i).getAddress().c_str(),danhSach.at(i).getPosition().c_str(),danhSach.at(i).getType().c_str(),danhSach.at(i).getLevelSalary());
	cout<<"\n|------------------------------------------------------------------------------------------------------------------|";
	}
	cout<<endl;
}

void Project1::Tim_kiem_nhan_su(PhongBan list,string key)
{
	vector<Staff> result;
	result = list.search_Staff(key);
	if(result.size()==0) cout<<"KHONG TIM THAY!"<<endl;
	else
	{
		system("cls");
		cout<<"KET QUA TIM KIEM"<<endl;
		Xem_nhan_su_hien_co(result);
	}
}

void Project1::Them_phong_ban_moi()
{
	string ten;
	string diachi;
	int choose=0;
	
	cin.ignore();
	Them_Moi:cout << "----------------------------------"<<endl;
	cout <<"THEM PHONG BAN MOI:"<<endl;
	do
	{	
		cout<<"(*) LA BAT BUOC"<<endl;
		cout<<"Ten phong ban(*): ";
		getline(cin,ten);
		
		//cin.ignore();
		cout<<"Dia chi(*): ";
		getline(cin,diachi);
	} while (ten =="" || diachi=="");
	
	Get_phong_ban_hien_co();
	int id181;
	if(this->danh_sach_phong_ban.size()==0)
	{
		id181=1;
	}
	for(int i=0;i<this->danh_sach_phong_ban.size();i++)
	{
		if((("  "+ten) == this->danh_sach_phong_ban.at(i).getTen()) && (("  "+diachi) == this->danh_sach_phong_ban.at(i).getDiaChi()) )
		{
			cout<<"PHONG BAN DA TON TAI"<<endl;
			getch();
			return;	
		}
		if(i==this->danh_sach_phong_ban.size()-1) id181 = this->danh_sach_phong_ban.at(i).getID()+1;
	}
	
	PhongBan temp(ten,diachi,id181);
	
	Menu_PhongBan:system("cls");
	cout <<"Phong ban: "<<temp.getTen()<<endl;
	cout <<"Dia chi: "<<temp.getDiaChi()<<endl;
	cout <<"ID:"<<temp.getID()<<endl;
	cout << "----------------------------------"<<endl;
	cout << "CHON MOT TRONG CAC CHUC NANG SAU:"<<endl;
	cout << "1.Them nhan su moi" << endl;
	cout << "2.Xem danh sach nhan su hien co" << endl;
	cout <<	"3.Tim kiem nhan su"<<endl;
	cout <<	"4.Xoa nhan su"<<endl;
	cout << "5.Save"<<endl;
	cout <<	"6.Tro lai"<<endl;
	enter115:cout << "Enter an integer: ";
	scanf("%d",&choose);
	if(choose!=0)
		switch (choose)
		{
			case 1: //Them nhan su moi
			{
				system("cls");
				cout << "----------------------------------"<<endl;
				cout <<"Phong ban: "<<temp.getTen()<<endl;
				cout <<"Dia chi: "<<temp.getDiaChi()<<endl;
				cout <<"ID:"<<temp.getID()<<endl;
				cout << "----------------------------------"<<endl;
				cout <<"THEM NHAN SU MOI"<<endl;
				int lastid;
				
				if(temp.get_so_luong_nhan_su()==0) lastid =0;
				else lastid = temp.getNhanSu().at(temp.get_so_luong_nhan_su()-1).getID()%10000;

				temp.add_staff(Them_nhan_su_moi(temp.getID(),lastid+1));
				cout<<"THEM THANH CONG"<<endl;
				getch();
				goto Menu_PhongBan;
			}
			case 2: //Xem danh sach nhan su hien co
			{	
				system("cls");
				Xem_nhan_su_hien_co(temp.getNhanSu());
				getch();
				goto Menu_PhongBan;
			}
			case 3: //Tim kiem nhan su
			{
				string key;
				
				cin.ignore();
								
				cout << "----------------------------------"<<endl;
				cout<<"TIM KIEM NHAN VIEN"<<endl;
				cout<<"ENTER KEYWORD: ";				
				getline(cin,key);			
				Tim_kiem_nhan_su(temp,key);
				getch();
				goto Menu_PhongBan;
			}
			Xoa_Nhan_su:case 4: //Xoa nhan su
			{
				
				if(temp.getNhanSu().size()==0)
				{
					cout<<"CHUA CO NHAN SU NAO!"<<endl;
					getch();
					goto Menu_PhongBan;
				}
				else
				{
					system("cls");
					Xem_nhan_su_hien_co(temp.getNhanSu());
					int choose_delete=0;
					cout << "----------------------------------"<<endl;
					cout << "CHON MOT TRONG CAC CHUC NANG SAU:"<<endl;
					cout << "1.Nhap ID nhan su de xoa"<<endl;
					cout << "2.Tim kiem"<<endl;
					cout << "3.Tro lai"<<endl;
					enter161:cout << "Enter an integer: ";
					scanf("%d",&choose_delete);
					if(choose_delete!=0)
						switch(choose_delete)
						{
							case 1://Nhap ID nhan su de xoa
							{
								int index;
								cout << "----------------------------------"<<endl;
								cout << "XOA NHAN SU"<<endl;
								cout <<"Nhap ID: ";
								cin>>index;
								
								if(temp.remove_Staff(index))
								{
									cout<<"Xoa thanh cong"<<endl;
									goto Xoa_Nhan_su;
								}
								else cout<<"ID khong hop le"<<endl;
								getch();
								cin.ignore();
								goto Menu_PhongBan;
							}
							case 2://Tim kiem
							{
								string key;
						
								cin.ignore();
								cout << "----------------------------------"<<endl;
								cout << "TIM KIEM NHAN SU"<<endl;
								cout<<"Nhap tu khoa de tim kiem: ";
								getline(cin,key);			
								
								vector<Staff> result = temp.search_Staff(key);
								if(result.size()==0)
								{
									cout<<"KHONG TIM THAY!"<<endl;
									goto Xoa_Nhan_su;
								}
								else
								{
									system("cls");
									cout << "----------------------------------"<<endl;
									cout<<"KET QUA TIM KIEM"<<endl;
									Xem_nhan_su_hien_co(result);
									int index;
									cout << "----------------------------------"<<endl;
									cout <<"Nhap ID de xoa: ";
									scanf("%d",&index);
									for(int i=0;i<result.size();i++)
									if(index == result.at(i).getID())
									{
										if(temp.remove_Staff(index))
										{
											cout<<"XOA THANH CONG!"<<endl;
											goto Xoa_Nhan_su;
										}
									}
									else 
									{
										cout<<"ID KHONG HOP LE!"<<endl;
										getch();
										cin.ignore();
										goto Xoa_Nhan_su;
									}
								}
							}
							case 3: 
							goto Menu_PhongBan;
							default : 
							{
								goto Xoa_Nhan_su;
							}
						}
						else 
						{
							cin.ignore();
							goto enter161;
						}
				}
				
			}
			case 5: //Save
			{
				if(temp.get_so_luong_nhan_su()!=0)
				{
					this->danh_sach_phong_ban.push_back(temp);
					save_Data();
					cout<<"SAVE THANH CONG"<<endl;
					getch();
					goto Menu_PhongBan;
				}
				else 
				{
					cout<<"CHUA CO NHAN SU NAO"<<endl;
					getch();
					goto Menu_PhongBan;
				}
				
				
			}
			case 6:
			{
				break;				
			}
			default : goto Menu_PhongBan;
		}
	else 
		{
			cin.ignore();
			goto enter115;
		}
}

void  Project1::Get_phong_ban_hien_co()
{
	
	vector<PhongBan> list_phong_ban;

	ifstream infile; 
	infile.open("staff.dat",ios::in);
	if(infile == NULL) return;
	while(!infile.eof())
	{
		PhongBan B; 
		B.get_Data_Staff_From_File(infile);
		//cout<<B.getID()<<endl;
		if(B.getID()==0) break;
		//B.showData();
		list_phong_ban.push_back(B);
		//cout<<list_phong_ban.size()<<endl;
	}
	infile.close();
	//cout<<list_phong_ban.size()<<endl;
	set_danh_sach(list_phong_ban);
}

void  Project1::show_data_phong_ban()
{
	if(this->danh_sach_phong_ban.size()==0)
	{
		cout<<"CHUA CO PHONG BAN NAO!"<<endl;
	}
	else
	{
		cout<<"|----------------------------------------------------------------------------------------------|"<<endl;
		cout<<"|					DANH SACH PHONG BAN		      		       |"<<endl;
		cout<<"|----------------------------------------------------------------------------------------------|"<<endl;
		printf("|%-9s | %-30s | %-30s | %-15s |","ID","Phong Ban","Dia chi","Tong luong");
		cout<<"\n|----------------------------------------------------------------------------------------------|";
		for(int i =0;i<this->danh_sach_phong_ban.size();i++)
		{	
		
		printf("\n|%-9d | %-30s | %-30s | %-15ld |",this->danh_sach_phong_ban.at(i).getID(),(this->danh_sach_phong_ban.at(i).getTen()).c_str(),(this->danh_sach_phong_ban.at(i).getDiaChi()).c_str(),this->danh_sach_phong_ban.at(i).get_sum_salary());
		cout<<"\n|----------------------------------------------------------------------------------------------|";
		}
		cout<<"\n|----------------------------------------------------------------------------------------------|"<<endl;

	}
}

void Project1::Thong_Tin_Chi_Tiet(PhongBan temp)
{
	cout<<"---------------------------------------------"<<endl;
	cout <<"Phong ban: "<<temp.getTen()<<endl;
	cout <<"Dia chi: "<<temp.getDiaChi()<<endl;
	cout <<"ID:"<<temp.getID()<<endl;
	vector<Staff> danhSach = temp.getNhanSu();
	cout<<"|----------------------------------------------------|"<<endl;
	cout<<"|		      BANG LUONG 		     |"<<endl;
	cout<<"|----------------------------------------------------|"<<endl;
	printf("|%-9s | %-20s | %-15s  |","ID","ho va ten","luong");
	cout<<"\n|----------------------------------------------------|";
	for(int i=0;i<danhSach.size();i++)
	{
		printf("\n|%-9d | %-20s | %-15ld  |",danhSach.at(i).getID(),danhSach.at(i).getFullName().c_str(),danhSach.at(i).salary());
		if(i==danhSach.size()-1)
		{
			cout<<"\n|----------------------------------------------------|";
			printf("\n|Tong: 				    %-15ld  |",temp.get_sum_salary());
		}
		cout<<"\n|----------------------------------------------------|";
	}
	cout<<endl;
}

void Project1::Menu_Nhan_Vien(PhongBan temp)
{
	Menu_NV:system("cls");
	Thong_Tin_Chi_Tiet(temp);
	cout << "\n\n----------------------------------"<<endl;
	cout << "CHON MOT TRONG CAC CHUC NANG SAU:"<<endl;
	cout << "1.Them Nhan vien"<<endl;
	cout << "2.Nhap ID Nhan vien de xem"<<endl;
	cout << "3.Tim Kiem nhan vien"<<endl;
	cout << "4.Xoa Nhan vien"<<endl;
	cout << "5.Sua Thong tin Nhan Vien"<<endl;
	cout << "6.Xap xep nhan vien"<<endl;
	cout << "7.Tro lai"<<endl;
	enter373:cout << "Enter an integer: ";
	int choose374 = 0;
	scanf("%d",&choose374);
	if(choose374!=0)
	{
		switch(choose374)
		{
			case 1://Them Nhan vien
			{
				system("cls");
				cout << "----------------------------------"<<endl;
				cout<<"THEM NHAN VIEN"<<endl;
				Staff newbie;
				newbie = Them_nhan_su_moi(temp.getID(),temp.getNhanSu().at(temp.get_so_luong_nhan_su()-1).getID()%10000+1);
				temp.add_staff(newbie);
				
				for(int i=0;i<this->danh_sach_phong_ban.size();i++)
				{
					if(this->danh_sach_phong_ban.at(i).getID()== temp.getID())
					this->danh_sach_phong_ban.erase(this->danh_sach_phong_ban.begin() + i);
				}
				
				this->danh_sach_phong_ban.push_back(temp);
				save_Data();
				
				cout<<"THEM THANH CONG"<<endl;
				getch();
				goto Menu_NV;
			}
			case 2: // xem thong tin nhan vien
			{
				
				cout << "----------------------------------"<<endl;
				cout<<"XEM THONG TIN NHAN VIEN"<<endl;
				cout<<"Enter ID: ";
				int id;
				scanf("%d",&id);
				for(int i=0;i<temp.get_so_luong_nhan_su();i++)
				{
					if(id == temp.getNhanSu().at(i).getID())
					{
						system("cls");
						cout << "----------------------------------"<<endl;
						cout<<"THONG TIN NHAN VIEN"<<endl;
						temp.getNhanSu().at(i).display();
						getch();
						goto Menu_NV;
					}
					if(i == temp.get_so_luong_nhan_su()-1)
					{
						cout<<"ID KHONG HOP LE"<<endl;
						getch();
						cin.ignore();
						goto Menu_NV;
					}
				}
			}
			case 3: // tim kiem nhan vien
			{
				
				cout << "----------------------------------"<<endl;
				cout<<"TIM KIEM NHAN VIEN"<<endl;
				cout<<"ENTER KEYWORD: ";
				string key;
				cin>>key;
				vector<Staff> result = temp.search_Staff(key);			
				if(result.size()!=0)
				{
					system("cls");
					cout<<"KET QUA TIM KIEM:"<<endl;
					Xem_nhan_su_hien_co(result);
				}
				else cout<<"KHONG TIM THAY"<<endl;
				getch();
				goto Menu_NV;
			}				
			case 4: // xoa nhan vien
			{
				cout << "----------------------------------"<<endl;
				cout<<"XOA NHAN VIEN"<<endl;
				cout<<"Enter ID: ";
				int id;
				scanf("%d",&id);
				if(temp.remove_Staff(id))
				{
					cout<<"XOA THANH CONG"<<endl;
					
					for(int i=0;i<this->danh_sach_phong_ban.size();i++)
					{
						if(this->danh_sach_phong_ban.at(i).getID()== temp.getID())
						this->danh_sach_phong_ban.erase(this->danh_sach_phong_ban.begin() + i);
					}
					
					this->danh_sach_phong_ban.push_back(temp);
					save_Data();
					getch();
					goto Menu_NV;
				}
				else
				{
					cout<<"ID KHONG HOP LE"<<endl;
					getch();
					cin.ignore();
					goto Menu_NV;
				}
			}
			case 5: //update thong tin
			{
				cout << "----------------------------------"<<endl;
				cout<<"UPDATE THONG TIN "<<endl;
				cout<<"Enter ID: ";
				int id;
				Staff change;
				scanf("%d",&id);
				for(int i =0;i<temp.get_so_luong_nhan_su();i++)
				{
					if(id == temp.getNhanSu().at(i).getID())
					{
						change =temp.getNhanSu().at(i);
						temp.remove_Staff(id);
						break;
					}
				}
				if(change.getID()!=0)
				{
					system("cls");
					cout << "----------------------------------"<<endl;
					cout<<"CAP NHAP THONG TIN"<<endl;
					cout << "----------------------------------"<<endl;
					cout<<"THONG TIN NHAN VIEN"<<endl;
					change.display();
					cout << "\n----------------------------------"<<endl;
					cout<<"THONG TIN MOI"<<endl;
					string 	phoneNumber;
					string 	address;
					string 	position;
					string 	type;
					int 	levelSalary;
					char character;
					cin.ignore();
					
					cout<<"\nSo dien thoai: ";	getline(cin,phoneNumber);
					cout<<"\nDia chi: ";		getline(cin,address);
					cout<<"\nVi tri: ";			getline(cin,position);
					
					do // type
					{
						cout<<"\nLoai(CGCC,A31,A32,A21,A22,A1,A0,B,C1,C2,C3): ";getline(cin,type);
						if(type=="")
						{
							type = change.getType();
							break;
						}
						
					} while(type!="CGCC"&&type!="A31"&&type!="A32"&&type!="A21"&&type!="A22"&&type!="A1"&&type!="A0"&&type!="B"&&type!="C1"&&type!="C2"&&type!="C3");
													
					//cin.ignore();
					BacLuong592:cout<<"\nBac luong: ";	character = getchar();;
					
					if(character == '\n') goto select659;
					else levelSalary = (int)character - 48;
					
					if(type=="CGCC")
					{
						if(levelSalary<=0||levelSalary>3) 
						{
							cout<<"BAC LUONG KHONG HOP LE"<<endl;
							goto BacLuong592;
						}
					}
					else if(type=="A31"||type=="A32")
					{
						if(levelSalary<=0||levelSalary>6)
						{
							cout<<"BAC LUONG KHONG HOP LE"<<endl;
							goto BacLuong592;
						}
					}
					else if(type=="A21"||type=="A22")
					{
						if(levelSalary<=0||levelSalary>8)
						{ 
							cout<<"BAC LUONG KHONG HOP LE"<<endl;
							goto BacLuong592;
						}
					}
					else if(type=="A1")
					{
						if(levelSalary<=0||levelSalary>9)
						{
							cout<<"BAC LUONG KHONG HOP LE"<<endl;
							goto BacLuong592;
						}
					}
					else if(type=="A0")
					{
						if(levelSalary<=0||levelSalary>10)
						{			
							cout<<"BAC LUONG KHONG HOP LE"<<endl;
							goto BacLuong592;
						}
					}
					else if(type=="B"||type=="C1"||type=="C2"||type=="C3")
					{
						if(levelSalary<0||levelSalary>12)
						{
							cout<<"BAC LUONG KHONG HOP LE"<<endl;
							goto BacLuong592;
						}
					}
					
					select659:cin.ignore();
					select528:cout<<"ARE YOU SURE? (Y/N): ";
					char select;
					scanf("%c",&select);
					if(select=='Y'||select=='y')
					{
							
						if(phoneNumber!="")
						change.setPhoneNumber(phoneNumber);
						//else change.setPhoneNumber (temp.getphoneNumber());
						
						if(address!="")
						change.setAdress(address);
						//else change.setAdress(temp.getAddress());
						
						if(position!="")					
						change.setPosition(position);
						//else change.setPosition(temp.getPosition());
					
						if(type!="")
						change.setType(type);
						//else change.setType(temp.getType());
					
						if(levelSalary!=0)					
						change.setLevelSalary(levelSalary);
						//else change.setLevelSalary(temp.getLevelSalary());
					
						cout<<"THAY DOI THANH CONG"<<endl;
						getch();
					}
					else if(select!='N'&&select!='n'&&select!='Y'&&select!='y') 
					{
						getch();
						cin.ignore();
						goto select528;
					}	
					temp.add_staff(change);
					
					for(int i=0;i<this->danh_sach_phong_ban.size();i++)
					{
						if(this->danh_sach_phong_ban.at(i).getID()== temp.getID())
						this->danh_sach_phong_ban.erase(this->danh_sach_phong_ban.begin() + i);
					}
					
					this->danh_sach_phong_ban.push_back(temp);
					save_Data();
					goto Menu_NV;
				}
				else
				{
					cout<<"ID KHONG HOP LE"<<endl;
					getch();
					cin.ignore();
					goto Menu_NV;
				}
			}
			case 6: //sap xep nhan vien
			{
				Menu_NV668:system("cls");
				cout << "----------------------------------"<<endl;
				cout<<"SAP XEP NHAN VIEN"<<endl;
				cout<<"1.Sap xep luong tang dan"<<endl;
				cout<<"2.Sap xep luong giam dan"<<endl;
				cout<<"3.Thoat"<<endl;
				enter674:cout << "Enter an integer: ";
				int choose675 = 0;
				scanf("%d",&choose675);
				if(choose675!=0)
				{
					vector<Staff> result = temp.getNhanSu();
					switch(choose675)
					{
						case 1: //Sap xep luong tang dan
						{
							
							for(int i=0;i<result.size()-1;i++)
								for(int j=i+1;j<result.size();j++)
								{
									if(result.at(i).salary()>result.at(j).salary())
									{
										Staff swap = result.at(i);
										result.at(i) = result.at(j);
										result.at(j) = swap;
									}
								}
							system("cls");
							temp.setNhanSu(result);
							Thong_Tin_Chi_Tiet(temp);
							getch();
							goto Menu_NV;
						}
						case 2: //Sap xep luong giam dan
						{
							
							for(int i=0;i<result.size()-1;i++)
								for(int j=i+1;j<result.size();j++)
								{
									if(result.at(i).salary()<result.at(j).salary())
									{
										Staff swap = result.at(i);
										result.at(i) = result.at(j);
										result.at(j) = swap;
									}
								}
							system("cls");
							temp.setNhanSu(result);
							Thong_Tin_Chi_Tiet(temp);
							getch();
							goto Menu_NV;
						}
						case 3:
						{
							system("cls");
							goto Menu_NV;
						}
						default:
						{
							goto Menu_NV668;
						}
					}
				}
				else
				{
					cin.ignore();
					goto enter674;
				}
			}
			case 7:
				system("cls");
				return;
			default: goto Menu_NV;
		}
	}
	else
	{
		cin.ignore();
		goto enter373;
	}
	
}
void Project1::Menu()
{
	
	menu:system("cls");
	
	cout << "--------CHUONG TRINH QUAN LY NHAN SU, TIEN LUONG---------" << endl;
	cout << "PROJECT1"<<endl;
	cout << "GV huong dan: PGS.TS Dang Van Chuyet"<<endl;
	cout << "Sinh vien thuc hien: Nguyen Van Hiep"<<endl;
	cout << "MSSV:20158142"<<endl;
	cout << "Lop: VUWIT15"<<endl;
	cout << "---------------------------------------------------------"<<endl;
	cout << "MENU CHINH:" << endl;
	cout << "1.Xem phong ban hien co" << endl;
	cout << "2.Sua phong ban"<<endl;
	cout << "3.Xoa phong ban"<<endl;
	cout << "4.Them phong ban moi" << endl;	
	cout <<	"5.Thoat"<<endl;
	enter300:cout << "Enter an integer: ";
	int choose =0;
	scanf("%d",&choose);
	if(choose!=0)
	{	switch (choose)
		{
			
			case 1: //Xem phong ban hien co
			{
				
				Get_phong_ban_hien_co();
				if(this->danh_sach_phong_ban.size()==0)
				{
					cout << "----------------------------------"<<endl;
					cout<<"CHUA CO PHONG BAN NAO"<<endl;
					getch();
					goto menu;
					
				}
				system("cls");
				menu702:show_data_phong_ban();
				getch();
				enter344:cout << "----------------------------------"<<endl;
				cout << "CHON MOT TRONG CAC CHUC NANG SAU:"<<endl;
				cout << "1.Nhap ID phong ban de xem"<<endl;
				cout << "2.Tro lai"<<endl;
				enter362:cout << "Enter an integer: ";
				int choose363 = 0;
				scanf("%d",&choose363); 
				if(choose363!=0)
				{
					switch (choose363)
					{
						case 1: //Nhap ID phong ban de xem
						{
							cout << "----------------------------------"<<endl;
							cout<<"XEM PHONG BAN"<<endl;
							cout << "Enter ID: ";
							int id;
							scanf("%d",&id);								
							for(int i=0;i<this->danh_sach_phong_ban.size();i++)
							{
								if(this->danh_sach_phong_ban.at(i).getID()==id)
								{
									
									
									Menu_Nhan_Vien(this->danh_sach_phong_ban.at(i));
									
									goto menu702;
								}
								else if(i == this->danh_sach_phong_ban.size()-1)
								{
									cout<<"ID KHONG HOP LE"<<endl;
									getch();
									cin.ignore();
									goto enter344;
							
								}
							}
							break;
						}
						
						case 2: 
							system("cls");
							goto menu;
							break;
						default: 
							goto enter344;
							break;
					}
				}
				else
				{	cin.ignore();							
					goto enter362;
				}
				break;

			}
			
			case 2: // Sua phong ban
			{
				Get_phong_ban_hien_co();
				if(this->danh_sach_phong_ban.size()==0)
				{
					cout << "----------------------------------"<<endl;
					cout<<"CHUA CO PHONG BAN NAO"<<endl;
					getch();
					goto menu;
					
				}
				cout << "----------------------------------"<<endl;
				cout <<"SUA PHONG BAN"<<endl;
				cout << "Enter ID: ";
				int id;
				scanf("%d",&id);
				for(int i=0;i<this->danh_sach_phong_ban.size();i++)
				{
					if(this->danh_sach_phong_ban.at(i).getID()==id)
					{
						system("cls");
						Menu_Nhan_Vien(danh_sach_phong_ban.at(i));
						save_Data();
						goto menu;
					}
					else if(i == this->danh_sach_phong_ban.size()-1)
					{
						cout<<"ID KHONG HOP LE"<<endl;
						getch();
						cin.ignore();
						goto menu;
					}
				}
			}
			case 3: //Xoa phong ban
			{
				Get_phong_ban_hien_co();
				if(this->danh_sach_phong_ban.size()==0)
				{
					cout << "----------------------------------"<<endl;
					cout<<"CHUA CO PHONG BAN NAO"<<endl;
					getch();
					goto menu;
					
				}
				cout << "----------------------------------"<<endl;
				cout <<"XOA PHONG BAN"<<endl;
				cout << "Enter ID: ";
				int id;
				scanf("%d",&id);
				for(int i=0;i<this->danh_sach_phong_ban.size();i++)
				{
					if(this->danh_sach_phong_ban.at(i).getID()==id)
					{
						system("cls");
						this->danh_sach_phong_ban.erase(this->danh_sach_phong_ban.begin() + i);
						cout<<"XOA THANH CONG"<<endl;
						save_Data();
						getch();
						goto menu;
					}
					else if(i == this->danh_sach_phong_ban.size()-1)
					{
						cout<<"ID KHONG HOP LE"<<endl;
						getch();
						cin.ignore();
						goto menu;
					}
				}
			}
			case 4: //Them phong ban moi
				system("cls");
				Them_phong_ban_moi();
				goto menu;
			case 5:
				system("cls");
				exit(1);
			default :
			{
				cin.ignore();
				system("cls");
				goto menu;
				break;
			}
		}
	}
	else 
	{
		cin.ignore();
		goto enter300;
		
	}

}

void	Project1::save_Data()
{
	ofstream outfile;
	outfile.open("staff.dat",ios::trunc);
	outfile <<"----------------------------------------		"<<endl;
	outfile.close();
	for(int i=0;i<this->danh_sach_phong_ban.size();i++)
		danh_sach_phong_ban.at(i).save_Staff_Data();
}

int main()
{
//g++ Project1.cpp phongBan.cpp staff.cpp -o Project1.exe

 	Project1 test;
	test.Menu();
	/*test.Get_phong_ban_hien_co();
	
	test.Thong_Tin_Chi_Tiet(test.get_danh_sach().at(0));
	
	PhongBan temp = test.get_danh_sach().at(1);
	cout<<"enter keyword: ";
	string key;
	cin>>key;
	vector<Staff> result = temp.search_Staff(key);
	
	if(result.size()!=0)
	test.Xem_nhan_su_hien_co(result);*/
	return 0;
}
