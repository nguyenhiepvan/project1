/*
- Bản quyền thuộc về SV. Nguyễn Văn Hiệp, Sinh viên Viện CNTT-TT, Trường ĐHBK Hà Nội.
- Đây là mã nguồn được tạo ra để hoàn thành bộ môn PROJECT1
- Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập , cần có sự cho phép của SV. N.V.Hiệp.
- Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền
- Khi sử dụng cần nói rõ nguồn sử dụng
- Email: nguyenhiepvan.bka@gmail.com
*/

#include <iostream>
#include <fstream>
#include <string>


using namespace std;
 
#ifndef STAFF_H
#define STAFF_H
	
class Staff
{
	
	
	private:
		int 	id;
		string 	fullName;
		string 	phoneNumber;
		string 	address;
		string 	position;
		string 	type;
		int 	levelSalary;
	public:	
		Staff();
		Staff(int,string ,string ,string ,string ,string ,int );
		void 	setID(int);
		void 	setFullName(string);	
		void 	setPhoneNumber(string);	
		void 	setAdress(string);	
		void 	setPosition(string);	
		void 	setType(string);	
		void 	setLevelSalary(int);
		int 	getID();
		string 	getFullName();		
		string 	getphoneNumber()	;	
		string 	getAddress();		
		string 	getPosition();		
		string 	getType();		
		int 	getLevelSalary();		
		float 	getSalary();
		long 	salary();
		void 	display();
		void 	getDataFromFile(ifstream&);
		void 	setDataToFileProflie();
		

};
#endif
